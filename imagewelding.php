<?php
/**
 * Created by PhpStorm.
 * User: dlipeev
 * Date: 02.11.15
 * Time: 14:06
 */
    DEFINE('NEW_FILES_STORAGE_PATH','./newfiles/');


    function returnError($text){
        $result=[
            'status'=>'ERROR',
            'msg'=>$text
        ];
        echo json_encode($result);
        die(0);
    }


    function loadImage($path,$is_url=false){
        if($is_url){
            $parsed_url=parse_url($path);
            $parsed_filename=pathinfo($parsed_url['path']);
            $ext=strtolower($parsed_filename['extension']);
        } else {
            $parsed_filename=pathinfo($path);
            $ext=strtolower($parsed_filename['extension']);
        }

        switch($ext){
            case 'jpg':
            case 'jpeg':
                $image=imagecreatefromjpeg($path);
                break;
            case 'gif':
                $image=imagecreatefromgif($path);
                break;
            case 'png':
                $image=imagecreatefrompng($path);
                break;
        }
        return $image;
    }

/**
        x_offset - white space top left corner coordinates
        y_offset
        x_end    - white space bottom right corner coordinates
        y_end
        background_image: "6-large_default.jpg"]
*/
    //1st dimension of this array is size code of the picture on the wall
    $presets=[
        //40x30cm
        1=>['x_offset'=>146, 'y_offset'=>84, 'x_end'=>220, 'y_end'=>200, 'background_image'=> "./example_images/6-large_default.jpg"],
        //60x40cm
        2=>['x_offset'=>146, 'y_offset'=>84, 'x_end'=>305, 'y_end'=>242, 'background_image'=> "./example_images/6-large_default.jpg"],
        //70x50cm
        3=>['x_offset'=>146, 'y_offset'=>84, 'x_end'=>305, 'y_end'=>242, 'background_image'=> "./example_images/6-large_default.jpg"],
        //80x60cm
        4=>['x_offset'=>146, 'y_offset'=>84, 'x_end'=>305, 'y_end'=>242, 'background_image'=> "./example_images/6-large_default.jpg"],
        //100x70cm
        5=>['x_offset'=>146, 'y_offset'=>84, 'x_end'=>305, 'y_end'=>242, 'background_image'=> "./example_images/6-large_default.jpg"],
        //120x80cm
        6=>['x_offset'=>146, 'y_offset'=>84, 'x_end'=>305, 'y_end'=>242, 'background_image'=> "./example_images/6-large_default.jpg"],
        //150x100cm
        7=>['x_offset'=>146, 'y_offset'=>84, 'x_end'=>305, 'y_end'=>242, 'background_image'=> "./example_images/6-large_default.jpg"]
    ];






    $src_img    =filter_var($_POST['src_img'],FILTER_SANITIZE_URL);
    $size_code  =(int)filter_var($_POST['size_code'],FILTER_SANITIZE_NUMBER_INT);
    $src_img_id =filter_var($_POST['src_img_id'],FILTER_SANITIZE_NUMBER_INT); //fotolia id
    if(empty($src_img) ||  empty($size_code) || empty($src_img_id)){
        returnError('wrong params');
    }
    if(!isset($presets[$size_code])){
        returnError('wrong size');
    }

    $x_offset         =$presets[$size_code]['x_offset'];
    $y_offset         =$presets[$size_code]['y_offset'];
    $x_end            =$presets[$size_code]['x_end'];
    $y_end            =$presets[$size_code]['y_end'];
    $background_image =$presets[$size_code]['background_image'];

    $image=loadImage($src_img,true);
    $bg_image=loadImage($background_image,false);

    //destination thumbnail size
    $dst_w=$x_end-$x_offset;
    $dst_h=$y_end-$y_offset;

    //get src image size from fotolid
    $imagesize    = getimagesize($src_img);
    $src_w = (int) $imagesize[0];
    $src_h = (int) $imagesize[1];

    $img_to_paste=imagecreatetruecolor($dst_w, $dst_h);
    imagecopyresized ($bg_image, $image, $x_offset    , $y_offset  , 0          , 0          , $dst_w , $dst_h , $src_w , $src_h );

    $fmp_file_name=NEW_FILES_STORAGE_PATH.md5(time().$src_img).'.png';
    imagepng($bg_image,$fmp_file_name,0);
    //imagepng($image,$fmp_file_name,0);
    $result=[
        'status'=>'OK',
        'url'=>$fmp_file_name
    ];

    echo json_encode($result);
    die(0);