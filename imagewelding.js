/**
 * Created by dlipeev on 02.11.15.
 */


/**
 * errors handler, implement it as you wish (if needed)
 */
function showError(msg){
    console.warn(msg);
}

/**
 *
 * @param src_img    - source img url
 * @param src_img_id - fotolia image id
 * @param size_code  - image size code
 * @param dst_img_id - destination <img> tag ID, where to put new image
 */
function weldImage(src_img,src_img_id,size_code,dst_img_id)
{
    if(typeof src_img == "undefined"    || src_img == "")    {showError('wrong params');return false};
    if(typeof dst_img_id == "undefined" || dst_img_id == "") {showError('wrong params');return false};



    jqxhr = $.ajax({
        method: "POST",
        url: "./imagewelding.php",
        data: {
            "src_img"   :src_img,
            "src_img_id":src_img_id,
            "size_code" :size_code
        }
    })
        .done(function (data) {
            data = JSON.parse(data);
            if (data['status'] == 'OK') {
                $('#'+dst_img_id).attr('src',data['url']);
            } else {
                showError('error');
            }
        })
        .fail(function () {
            showError('connection error');
        })
        .always(function () {

        });
}